var FILE_ENCODING = 'utf-8';
var fs = require('fs');
var path = require('path');
var withError = false;
console.log('[Translate] Concat files ...');

var dest = './src/assets/translations/';
EOL = '\n';
var en = {
    src: './src/assets/translations/en/',
    dst: 'en'
};
var fr = {
    src: './src/assets/translations/fr/',
    dst: 'fr'
};

function concat(opts) {
    var distPath = path.resolve(dest + opts.dst + '.json');
    console.log(path.resolve(distPath));
    var files = fs.readdirSync(opts.src);

    var out = files.map(function (filePath) {
        var temp = fs.readFileSync(opts.src + filePath, FILE_ENCODING);

        try {
            var jsonDs = JSON.parse(temp);
        } catch (e) {
            console.log('\u001b[' + 31 + 'm' + 'Error on file : ' + filePath + ' --> ' + e.message + '\u001b[0m')
            withError = true;
            return '';
        }

        temp = temp.substring(1, temp.length - 2)
        return '\t' + temp.trim() + ',';
    });

    var last = out[out.length - 1];
    last = last.substring(0, last.length - 1);
    out[out.length - 1] = last;
    fs.writeFileSync(distPath, '{\n' + out.join(EOL) + '\n}', FILE_ENCODING);
}

concat(en);
if (withError) {
    process.exit(1);
}
concat(fr);
if (withError) {
    process.exit(1);
}

console.log("[Translate] Concat end");
process.exit(1);