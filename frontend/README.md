# 🌱 [T-MAJ-800] Voltron

Specializing in the trading and processing of cereals, a company asked you to enable its customers (cereal growers, farmers . . . ) to view in real time, the state of their agricultural holdings, and to disseminate to them the data con- tinuously.

- **theme**: `greentech`
- **client**: a Céréales Vallée innovation manager
- **objective**: build a full-featured reporting tool for farmers

## 🏗️ Repository architecture

``` BASH 
.
├── README.md       -> documentation 
├── src             -> ionic source folder
├── package.json    -> dependencies of application
└── tslint.json     -> configurations of tslint
```

## 🚀 Run project

Run project localy :
```BASH
ionic serve
```

Run project on defined ip :
```BASH
ionic serve --host=192.168.xxx.xxx
```

Run project on defined port :
```BASH
ionic serve --port=xxxxx
```

Run tests :
```BASH
npm test
OR
npm run test-gitlab
```

Build project :
```BASH
npm run build
```

Build project :
```BASH
npm run build
```

## 📕 Documentation

Doc should be on 100%, comment with official documentation :
```ts
/**
 * [method description]
 * @param [param name] [param description]
 * @returns [return description]
 */
```

Example :
```ts
/**
 * Test method
 * @param test Test value
 * @returns return the test number
 */
 public test(test: number): number {
    return test;
 }
```

Generate documentation :
```BASH
npm run compodoc
```

## 🐳 Docker

Build :
```BASH
docker build -t t-web-800/frontend .
```

Run :
```BASH
docker run --name t-web-800-front -p 8100:8100 t-web-800/frontend
```

Stop :
```BASH
docker stop t-web-800-front
```

## 📝 Coding conventions

We use TSLint, use project convention defined on `tslint.json` file :
- Use `4 spaces indentations`
- Use `double quote` for string and import
- Specify method range (`public` / `private` / `protected`)
- Imports must be `alphabetised ordered`
- Imports must be `absolute path from src` ("src/.../...")
- `Empty new line` at end of file
