import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SensorPageComponent } from "src/app/sensor/sensor.page";

const routes: Routes = [
    {
        path: "",
        component: SensorPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class SensorPageRoutingModule { }
