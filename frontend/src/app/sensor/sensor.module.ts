import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { ChartsModule } from "ng2-charts";
import { SensorPageRoutingModule } from "src/app/sensor/sensor-routing.module";
import { SensorPageComponent } from "src/app/sensor/sensor.page";
import { SensorService } from "src/app/sensor/sensor.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SensorPageRoutingModule,
        TranslateModule,
        ChartsModule
    ],
    declarations: [
        SensorPageComponent
    ],
    providers: [
        SensorService,
        DatePipe
    ]
})
export class SensorPageModule { }
