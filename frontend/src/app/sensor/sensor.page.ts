import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AppPage } from "src/app/interfaces/iappPage";
import { ChartData } from "src/app/interfaces/ichart";
import { SensorService } from "src/app/sensor/sensor.service";

/**
 * Sensor page component
 */
@Component({
    selector: "app-sensor",
    templateUrl: "./sensor.page.html",
    styleUrls: ["./sensor.page.scss"],
})
export class SensorPageComponent implements OnInit {

    /**
     * Line chart options
     */
    public lineChartOptions = {
        scaleShowVerticalLines: false,
        responsive: true
    };

    /**
     * Chart datas
     */
    public chartData: ChartData;

    /**
     * Chart colors
     */
    public chartColors = [];

    /**
     * Actual sensor page
     */
    public sensorPage: AppPage;

    /**
     * Constructor of SensorPage component
     * @param router Router of application to get navigation extras state
     */
    constructor(
        private router: Router,
        private sensorService: SensorService
    ) {
        this.sensorPage = <AppPage>this.router.getCurrentNavigation().extras.state;
        if (!this.sensorPage) {
            this.router.navigate(["home"]);
        }
        this.chartColors = [
            {
                backgroundColor: this.sensorPage.color.replace(", 1)", ", 0.2)"),
                borderColor: this.sensorPage.color,
                pointBackgroundColor: this.sensorPage.color,
                pointBorderColor: "#FFFFFF",
                pointHoverBackgroundColor: "#FFFFFF",
                pointHoverBorderColor: this.sensorPage.color.replace(", 1)", ", 0.8)")
            }
        ];
    }

    /**
     * Method called on init of sensor page
     */
    public ngOnInit() {
        this.sensorService.getValues(this.sensorPage.serverSensorName).then((s: ChartData) => {
            this.chartData = s;
        });
    }

}
