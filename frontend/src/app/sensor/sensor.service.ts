import { DatePipe } from "@angular/common";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { orderBy, takeRight } from "lodash";
import { ChartData, ServerSensorData, ServerSensorDatas } from "src/app/interfaces/ichart";
import { BaseService } from "src/app/services/base-service.service";

/**
 * Sensor service to get sensor values
 */
@Injectable()
export class SensorService extends BaseService {

    /**
     * Constructor of SensorService
     * @param http HttpClient for BaseService
     */
    constructor(
        http: HttpClient,
        private translateService: TranslateService,
        private datepipe: DatePipe
    ) {
        super(http, "data");
    }

    /**
     * Get the humidity value for Chart
     * @param sensor name of sensor ("Photoresistor", "TempHumiditySensor", "waterLevelSensor", "flameSensor")
     */
    public async getValues(sensor: string): Promise<ChartData> {
        let onError = false;
        let result = null;
        await this._get<ServerSensorDatas>(`?sensor=${ sensor }`).toPromise().then((s: ServerSensorDatas) => {
            result = s;
        }, (err) => {
            onError = true;
        });
        if (onError) {
            throw new Error("Reject");
        }
        return new Promise<ChartData>((success, err) => {
            const values = {
                labels: [],
                serie: [
                    {
                        data: [],
                        label: ""
                    }
                ]
            };
            switch (sensor) {
                case "Photoresistor":
                    values.serie[0].label = this.translateService.instant("CHARTS.TYPES_VALUES.BRIGHTNESS");
                    break;

                case "TempHumiditySensor":
                    values.serie[0].label = this.translateService.instant("CHARTS.TYPES_VALUES.HUMIDITY");
                    break;

                case "waterLevelSensor":
                    values.serie[0].label = this.translateService.instant("CHARTS.TYPES_VALUES.WATER_LEVEL");
                    break;

                case "flameSensor":
                    values.serie[0].label = this.translateService.instant("CHARTS.TYPES_VALUES.FIRE_DETECTION");
                    break;
            }
            takeRight(orderBy(result.data, ["time"], ["asc"]), 7).forEach((serverValue: ServerSensorData) => {
                values.labels.push(this.datepipe.transform(serverValue.time, "HH:mm"));
                values.serie[0].data.push(serverValue.data);
            });
            success(values);
        });
    }

}
