import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SensorPageComponent } from "src/app/sensor/sensor.page";

describe("SensorPage", () => {

    let component: SensorPageComponent;
    let fixture: ComponentFixture<SensorPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                SensorPageComponent
            ],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot([])
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(SensorPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
