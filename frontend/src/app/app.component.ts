import { Component, OnInit } from "@angular/core";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Platform } from "@ionic/angular";
import { AppPage } from "src/app/interfaces/iappPage";
import { UserAuth } from "src/app/interfaces/iauth";
import { AppParameters } from "src/app/interfaces/iparameters";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user.service";


/**
 * Main component of application
 */
@Component({
    selector: "app-root",
    templateUrl: "app.component.html",
    styleUrls: ["app.component.scss"]
})
export class AppComponent implements OnInit {

    /**
     * Selected index for menu
     */
    public selectedIndex = 0;
    /**
     * This variable will contains our app page for menu
     */
    public appPages: AppPage[] = [
        {
            title: "HOME",
            url: "/home",
            icon: "home",
            color: "#FFFFFF"
        },
        {
            title: "HUMIDITY",
            url: "/sensor/humidity",
            icon: "water",
            color: "rgba(3, 169, 244, 1)",
            serverSensorName: "TempHumiditySensor"
        },
        {
            title: "BRIGHTNESS",
            url: "/sensor/brightness",
            icon: "sunny",
            color: "rgba(255, 193, 7, 1)",
            serverSensorName: "Photoresistor"
        },
        {
            title: "WATER_LEVEL",
            url: "/sensor/water-level",
            icon: "podium",
            color: "rgba(102, 193, 57, 1)",
            serverSensorName: "waterLevelSensor"
        },
        {
            title: "FIRE_DETECTION",
            url: "/sensor/fire-detection",
            icon: "bonfire",
            color: "rgba(244, 67, 54, 1)",
            serverSensorName: "flameSensor"
        },
        {
            title: "PARAMS",
            url: "/params",
            icon: "settings",
            color: "rgba(255, 255, 255, 1)"
        },
    ];

    /**
     * Constructor of AppComponent
     * @param platform Platform to know when application is ready to use
     * @param splashScreen Show a splash screen
     * @param statusBar Set status bar to default style
     * @param translateService Translate service to set default language of application
     * @param userService User service to get default language
     * @param authService Auth service to get logged user
     */
    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private userService: UserService,
        private authService: AuthService
    ) {
        this.initializeApp();
    }

    /**
     * Called at start of application
     */
    public initializeApp(): void {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
        this.userService.getToStorage("appParameters").then((appParameters: AppParameters) => {
            if (appParameters) {
                this.userService.changeParameters(appParameters);
            } else {
                this.userService.changeParameters(new AppParameters({
                    interval: 1,
                    language: "en"
                }));
            }
        });

        this.userService.getToStorage("userAuth").then((userAuth: UserAuth) => {
            if (userAuth) {
                this.authService.userAuth.next(userAuth);
            }
        });
    }

    /**
     * On init of app page
     */
    public ngOnInit(): void {
        const path = window.location.pathname.split("sensor/")[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex((page) => page.title.toLowerCase() === path.toLowerCase());
        }
    }

}
