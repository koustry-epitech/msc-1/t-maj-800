import { SensorList } from "src/app/interfaces/isensor";

/**
 * Enum for notification type (used to define icon in home)
 */
export enum NotificationType {

    /**
     * Error type
     */
    ERROR,

    /**
     * Warning type
     */
    WARNING

}

/**
 * Notification class
 */
export class Notification {

    /**
     * Type of Notification
     */
    public notificationType: NotificationType;

    /**
     * Message of Notification
     */
    public msg: string;

    /**
     * Date of Notification
     */
    public date: Date;

    /**
     * Sensor of Notification
     */
    public sensor: SensorList;

    /**
     * Constructor of Notification
     * @param obj Object to initialize our Notification class
     */
    constructor(obj: Notification) {
        this.notificationType = obj.notificationType;
        this.msg = obj.msg;
        this.date = obj.date;
        this.sensor = obj.sensor;
    }

}
