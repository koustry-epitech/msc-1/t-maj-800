/**
 * App parameters class
 */
export class AppParameters {

    /**
     * Language of application
     */
    public language: string;
    /**
     * Interval to get server data
     */
    public interval: number;

    /**
     * Cosntructor of AppParameters class
     * @param obj Object to initialize AppParameters class
     */
    constructor(obj: AppParameters) {
        this.language = obj.language;
        this.interval = obj.interval;
    }

}
