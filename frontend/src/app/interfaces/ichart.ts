/**
 * Serie class
 */
export class Serie {

    /**
     * Datas of serie
     */
    public data: string[];

    /**
     * Label of serie
     */
    public label: string;

    /**
     * Constructor of Serie class
     * @param obj Object to initialize Serie class
     */
    constructor(obj: Serie) {
        this.data = obj.data;
        this.label = obj.label;
    }

}

/**
 * ChartData class
 */
export class ChartData {

    /**
     * Labels of chart (absissa)
     */
    public labels: string[];

    /**
     * Serie (data) of chart
     */
    public serie: Serie[];

    /**
     * Constructor of ChartData class
     * @param obj Object to initialize ChartData class
     */
    constructor(obj: ChartData) {
        this.labels = obj.labels;
        this.serie = [];
        if (obj.serie) {
            obj.serie.forEach((serie) => {
                this.serie.push(new Serie(serie));
            });
        }
    }

}

/**
 * ServerSensorDatas class
 */
export class ServerSensorDatas {

    /**
     * Data of ServerSensorDatas
     */
    public data: ServerSensorData[];

    /**
     * Constructor of ServerSensorDatas class
     * @param obj Object to initialize ServerSensorDatas class
     */
    constructor(obj: ServerSensorDatas) {
        this.data = obj.data;
    }

}

/**
 * ServerSensorData class
 */
export class ServerSensorData {

    /**
     * Id of ServerSensorData
     */
    public _id: string;
    /**
     * Sensor of ServerSensorData
     */
    public sensor: string;
    /**
     * Date of ServerSensorData
     */
    public time: Date;
    /**
     * Data of ServerSensorData
     */
    public data: string;

    /**
     * Constructor of ServerSensorData class
     * @param obj Object to initialize ServerSensorData class
     */
    constructor(obj: ServerSensorData) {
        this._id = obj._id;
        this.sensor = obj.sensor;
        this.time = new Date(obj.time);
        this.data = obj.data;
    }

}
