/**
 * Language class
 */
export class Language {

    /**
     * Id of language
     * ex: en, fr, es ...
     */
    public id: string;

    /**
     * Translate code of langage
     * ex: EN, FR, ES ...
     */
    public translateCode: string;

    /**
     * Cosntructor of Language class
     * @param obj Object to initialize Language class
     */
    constructor(obj: Language) {
        this.id = obj.id;
        this.translateCode = obj.translateCode;
    }

}
