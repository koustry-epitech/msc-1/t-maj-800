/**
 * User class
 */
export class User {

    /**
     * Name of user
     */
    public name: string;
    /**
     * Email of user
     */
    public email: string;
    /**
     * Password of user
     */
    public password: string;

    /**
     * Constructor of User class
     * @param obj User to create our class
     */
    constructor(obj: User) {
        this.name = obj.name;
        this.email = obj.email;
        this.password = obj.password;
    }

}

/**
 * UserAuth class
 */
export class UserAuth {

    /**
     * Token of user auth
     */
    public token: string;
    /**
     * Auth type of user ("Bearer", ...)
     */
    public authType: string;
    /**
     * Name of user auth
     */
    public name: string;

    /**
     * Constructor of UserAuth class
     * @param obj User to create our class
     */
    constructor(obj: UserAuth) {
        this.token = obj.token;
        this.authType = obj.authType;
        this.name = obj.name;
    }

}

/**
 * LoginInfo class
 */
export class LoginInfo {

    /**
     * Username to login (email)
     */
    public username: string;
    /**
     * Password to login
     */
    public password: string;

    /**
     * Constructor of LoginInfo class
     * @param obj object to intialize our LoginInfo class
     */
    constructor(obj: LoginInfo) {
        this.username = obj.username;
        this.password = obj.password;
    }

}
