/**
 * Sensor list enum (used for menu)
 */
export enum SensorList {

    /**
     * Humidity sensor
     */
    HUMIDITY,
    /**
     * Brightness sensor
     */
    BRIGHTNESS,
    /**
     * Water level sensor
     */
    WATER_LEVEL,
    /**
     * Fire detection sensor
     */
    FIRE_DETECTION

}

/**
 * Sensor data class
 */
export class SensorData {

    /**
     * Type of sensor
     */
    public sensor: SensorList;
    /**
     * Value of sensor
     */
    public value: number | string | boolean;
    /**
     * Unity of value
     */
    public unity = "";

    /**
     * Cosntructor of SensorData
     * @param obj Object to initialize SensorData class
     */
    constructor(obj: SensorData) {
        this.sensor = obj.sensor;
        this.value = obj.value;
        this.unity = obj.unity;
    }

}
