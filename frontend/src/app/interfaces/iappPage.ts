/**
 * App page class
 */
export class AppPage {

    /**
     * Title of AppPage
     */
    public title: string;
    /**
     * Url of AppPage
     */
    public url: string;
    /**
     * Icon of AppPage
     */
    public icon: string;
    /**
     * Color of AppPage
     */
    public color: string;
    /**
     * Name of sensor server side
     */
    public serverSensorName?: string;

    /**
     * Constructor of AppPage
     * @param obj object to initialise our AppPage class
     */
    constructor(obj: AppPage) {
        this.title = obj.title;
        this.url = obj.url;
        this.icon = obj.icon;
        this.color = obj.color;
        this.serverSensorName = obj.serverSensorName;
    }

}
