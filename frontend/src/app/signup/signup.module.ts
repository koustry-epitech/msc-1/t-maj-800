import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { AuthService } from "src/app/services/auth.service";
import { SignupPageRoutingModule } from "src/app/signup/signup-routing.module";
import { SignupPageComponent } from "src/app/signup/signup.page";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SignupPageRoutingModule,
        TranslateModule
    ],
    declarations: [
        SignupPageComponent
    ],
    providers: [
        AuthService,
    ]
})
export class SignupPageModule { }
