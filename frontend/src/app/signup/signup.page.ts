import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { User, UserAuth } from "src/app/interfaces/iauth";
import { AuthService } from "src/app/services/auth.service";
import { ErrorInterceptor } from "src/app/services/errors.interceptor";

/**
 * Signup page component
 */
@Component({
    selector: "app-signup",
    templateUrl: "./signup.page.html",
    styleUrls: ["./signup.page.scss"],
})
export class SignupPageComponent {

    /**
     * Login info
     */
    public signupInfo: User = new User({
        name: "",
        email: "",
        password: ""
    });

    /**
     * Constructor of SigninPageComponent
     * @param authService Auth service to set userAuth value
     * @param storage Storage of application to set userAuth value
     * @param router Router to navigate to home when user is logged
     */
    constructor(
        private authService: AuthService,
        private storage: Storage,
        private router: Router
    ) { }

    /**
     * Do signup on API
     */
    public doSignup() {
        this.authService.signup(this.signupInfo).subscribe((s: UserAuth) => {
            this.authService.userAuth.next(s);
            this.storage.set("userAuth", s).then(() => {
                ErrorInterceptor.forbiddenDetected = false;
                this.router.navigateByUrl("home");
            });
        });
    }

    /**
     * Method to redirect
     * @param url url to redirect
     */
    public redirect(url: string) {
        this.router.navigateByUrl(url);
    }

}
