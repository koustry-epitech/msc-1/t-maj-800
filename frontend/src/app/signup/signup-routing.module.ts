import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SignupPageComponent } from "src/app/signup/signup.page";

/**
 * Routes of our applications
 */
const routes: Routes = [
    {
        path: "",
        component: SignupPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class SignupPageRoutingModule { }
