import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SignupPageComponent } from "src/app/signin/signin.page";

describe("SignupPage", () => {

    let component: SignupPageComponent;
    let fixture: ComponentFixture<SignupPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                SignupPageComponent
            ],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot([])
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(SignupPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
