import { Pipe, PipeTransform } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

/**
 * Pipe for boolean (Yes or No)
 */
@Pipe({
    name: "boolean",
    pure: false
})
export class BooleanPipe implements PipeTransform {

    /**
     * Constructor of BooleanPipe
     * @param translateService Translate service to get translation
     */
    constructor(
        private translateService: TranslateService
    ) { }

    /**
     * Tranform method overrided from PipeTransform
     * @param value Value to transform
     */
    public transform(value: boolean): string {
        return this.translateService.instant("GLOBAL.VALUES.BOOLEAN." + (value ? "1" : "0"));
    }

}
