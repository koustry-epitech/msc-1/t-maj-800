import { Pipe, PipeTransform } from "@angular/core";
import { sortBy } from "lodash";
import { Notification } from "src/app/interfaces/inotifcation";

/**
 * Pipe for notifications (sort by notification type)
 */
@Pipe({
    name: "notifications",
    pure: false
})
export class NotificationsPipe implements PipeTransform {

    /**
     * Tranform method overrided from PipeTransform
     * @param items Liste of notifications to order
     */
    public transform(items: Notification[]): Notification[] {
        if (!items) {
            return items;
        }
        return sortBy(items, [(o: Notification) => o.notificationType]);
    }

}
