import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { SigninPageComponent } from "src/app/signin/signin.page";

describe("SigninPage", () => {

    let component: SigninPageComponent;
    let fixture: ComponentFixture<SigninPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                SigninPageComponent
            ],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot([])
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(SigninPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
