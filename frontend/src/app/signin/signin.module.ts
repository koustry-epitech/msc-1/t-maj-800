import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { AuthService } from "src/app/services/auth.service";
import { SigninPageRoutingModule } from "src/app/signin/signin-routing.module";
import { SigninPageComponent } from "src/app/signin/signin.page";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SigninPageRoutingModule,
        TranslateModule
    ],
    declarations: [
        SigninPageComponent
    ],
    providers: [
        AuthService,
    ]
})
export class SigninPageModule { }
