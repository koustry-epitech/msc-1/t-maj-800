import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "src/app/services/auth.guard";

/**
 * Routes of our application
 */
const routes: Routes = [
    {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
    },
    {
        path: "home",
        loadChildren: () => import("./home/home.module").then((m) => m.HomePageModule),
        canActivate: [AuthGuard]
    },
    {
        path: "sensor/:id",
        loadChildren: () => import("./sensor/sensor.module").then((m) => m.SensorPageModule),
        canActivate: [AuthGuard]
    },
    {
        path: "signin",
        loadChildren: () => import("./signin/signin.module").then((m) => m.SigninPageModule)
    },
    {
        path: "signup",
        loadChildren: () => import("./signup/signup.module").then((m) => m.SignupPageModule)
    },
    {
        path: "params",
        loadChildren: () => import("./parameters/parameters.module").then((m) => m.ParametersPageModule)
    },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
