import { Component, OnInit } from "@angular/core";
import { UserAuth } from "src/app/interfaces/iauth";
import { Language } from "src/app/interfaces/ilangue";
import { AppParameters } from "src/app/interfaces/iparameters";
import { AuthService } from "src/app/services/auth.service";
import { UserService } from "src/app/services/user.service";

/**
 * Parameters page component
 */
@Component({
    selector: "app-parameters",
    templateUrl: "./parameters.page.html",
    styleUrls: ["./parameters.page.scss"],
})
export class ParametersPageComponent implements OnInit {

    /**
     * Contains a list of available language
     */
    public availableLanguage: Language[] = [
        new Language({
            id: "en",
            translateCode: "EN"
        }),
        new Language({
            id: "fr",
            translateCode: "FR"
        })
    ];
    /**
     * User app parameters
     */
    public appParameters: AppParameters;
    /**
     * User auth to app
     */
    public userAuth: UserAuth;

    /**
     * Constructor of ParametersPage component
     * @param userService UserService to get app parameters
     * @param authService AuthService to get user auth to app
     */
    constructor(
        private userService: UserService,
        public authService: AuthService
    ) { }

    /**
     * Called on initialization of parameters page
     */
    public ngOnInit() {
        this.userService.appParameters.subscribe((appParameters: AppParameters) => {
            this.appParameters = appParameters;
        });
        this.authService.userAuth.subscribe((userAuth: UserAuth) => {
            this.userAuth = userAuth;
        });
    }

    /**
     * Called when select value change
     * @param event Value of select change
     */
    public onChangeLanguage(event: any) {
        this.userService.getToStorage("appParameters").then((appParameters: AppParameters) => {
            appParameters.language = event.detail.value;
            this.userService.changeParameters(appParameters);
        });
    }

    /**
     * Called when interval input value change
     * @param event Value
     */
    public onChangeInterval(event: any) {
        this.userService.getToStorage("appParameters").then((appParameters: AppParameters) => {
            appParameters.interval = event.detail.value;
            this.userService.changeParameters(appParameters);
        });
    }

    /**
     * Sign out of application
     */
    public doSignout() {
        this.authService.signout();
    }

}
