import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { ParametersPageComponent } from "src/app/parameters/parameters.page";

describe("ParametersPage", () => {

    let component: ParametersPageComponent;
    let fixture: ComponentFixture<ParametersPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                ParametersPageComponent
            ],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot([])
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(ParametersPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
