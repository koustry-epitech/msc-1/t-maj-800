import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ParametersPageComponent } from "src/app/parameters/parameters.page";

const routes: Routes = [
    {
        path: "",
        component: ParametersPageComponent
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ],
})
export class ParametersPageRoutingModule { }
