import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { ParametersPageRoutingModule } from "src/app/parameters/parameters-routing.module";
import { ParametersPageComponent } from "src/app/parameters/parameters.page";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ParametersPageRoutingModule,
        TranslateModule
    ],
    declarations: [
        ParametersPageComponent
    ],
    providers: [

    ]
})
export class ParametersPageModule { }
