import { Component, OnInit } from "@angular/core";
import { remove } from "lodash";
import { HomeService } from "src/app/home/home.service";
import { ChartData } from "src/app/interfaces/ichart";
import { Notification } from "src/app/interfaces/inotifcation";
import { SensorData, SensorList } from "src/app/interfaces/isensor";
import { SensorService } from "src/app/sensor/sensor.service";

/**
 * Home page component
 */
@Component({
    selector: "app-home",
    templateUrl: "./home.page.html",
    styleUrls: ["./home.page.scss"],
})
export class HomePageComponent implements OnInit {

    /**
     * Data will contains all notifications of server
     */
    public data: Notification[] = [];
    /**
     * Contains sensor notifications
     */
    public sensorNotification: SensorData[] = [];

    /**
     * Constructor of HomePageComponent
     */
    constructor(
        private homeService: HomeService,
        private sensorService: SensorService
    ) { }

    /**
     * Method called when home component initialize
     */
    public ngOnInit(): void {
        this.homeService.getValues().subscribe((s: Notification[]) => {
            this.data = s;
        });
        this.getSensorValue("TempHumiditySensor", SensorList.HUMIDITY, "cm3");
        this.getSensorValue("Photoresistor", SensorList.BRIGHTNESS, "lux");
        this.getSensorValue("waterLevelSensor", SensorList.WATER_LEVEL, "%");
        this.getSensorValue("flameSensor", SensorList.FIRE_DETECTION, "");
    }

    /**
     * Method to get value in server side
     * @param sensorName Name of sensor
     * @param sensorList Name of sensor in list
     * @param unity Unity of sensor
     */
    public getSensorValue(sensorName: string, sensorList: SensorList, unity: string): void {
        this.sensorService.getValues(sensorName).then((s: ChartData) => {
            this.sensorNotification.push(new SensorData({
                sensor: sensorList,
                value: s.serie[0].data[s.serie[0].data.length - 1],
                unity: unity
            }));
        }, (err) => {
            console.log(err);
        });
    }

    /**
     * Method called when user validate a notification
     * @param notif Notification validate
     */
    public validNotif(notif: Notification): void {
        remove(this.data, notif);
    }

    /**
     * Get type of value
     * @param value Value to get type
     */
    public getType(value: string | number | boolean): string {
        return typeof value;
    }

}
