import { async, ComponentFixture, TestBed } from "@angular/core/testing";
import { RouterModule } from "@angular/router";
import { IonicModule } from "@ionic/angular";
import { HomePageComponent } from "src/app/Home/Home.page";

describe("HomePage", () => {

    let component: HomePageComponent;
    let fixture: ComponentFixture<HomePageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                HomePageComponent
            ],
            imports: [
                IonicModule.forRoot(),
                RouterModule.forRoot([])
            ]
        }).compileComponents();

        fixture = TestBed.createComponent(HomePageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    }));

    it("should create", () => {
        expect(component).toBeTruthy();
    });

});
