import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Notification, NotificationType } from "src/app/interfaces/inotifcation";
import { SensorList } from "src/app/interfaces/isensor";
import { BaseService } from "src/app/services/base-service.service";

/**
 * Home service to get sensor values
 */
@Injectable()
export class HomeService extends BaseService {

    /**
     * Constructor of HomeService
     * @param http HttpClient for BaseService
     */
    constructor(http: HttpClient) {
        super(http, "home");
    }

    /**
     * Get the humidity value for Chart
     */
    public getValues(): Observable<Notification[]> {
        // return this._get<Notification[]>("notification");
        return new Observable<Notification[]>((subscriber) => {
            subscriber.next([
                {
                    notificationType: NotificationType.WARNING,
                    msg: "Humidity was too low",
                    date: new Date(),
                    sensor: SensorList.HUMIDITY
                },
                {
                    notificationType: NotificationType.ERROR,
                    msg: "Fire detected",
                    date: new Date(),
                    sensor: SensorList.FIRE_DETECTION
                },
                {
                    notificationType: NotificationType.WARNING,
                    msg: "Brightness was too hight",
                    date: new Date(),
                    sensor: SensorList.BRIGHTNESS
                }
            ]);
        });
    }

}
