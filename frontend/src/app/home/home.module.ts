import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { TranslateModule } from "@ngx-translate/core";
import { HomePageRoutingModule } from "src/app/home/home-routing.module";
import { HomePageComponent } from "src/app/home/home.page";
import { HomeService } from "src/app/home/home.service";
import { BooleanPipe } from "src/app/pipe/boolean.pipe";
import { NotificationsPipe } from "src/app/pipe/filterNotification.pipe";
import { SensorService } from "src/app/sensor/sensor.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        HomePageRoutingModule,
        TranslateModule
    ],
    declarations: [
        HomePageComponent,
        NotificationsPipe,
        BooleanPipe
    ],
    providers: [
        HomeService,
        SensorService,
        DatePipe
    ]
})
export class HomePageModule { }
