import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { UserAuth } from "src/app/interfaces/iauth";

/**
 * Service to check if user is logged (for rooting access)
 */
@Injectable()
export class AuthGuard implements CanActivate {

    /**
     * User authentificated
     */
    private userAuth: UserAuth;

    /**
     * Constructor of service
     * @param router Router to navigate
     * @param storage Storage of application to get user auth
     */
    constructor(
        private router: Router,
        private storage: Storage
    ) {
    }

    /**
     * Method to check if user is logged
     */
    public async canActivate(): Promise<boolean> {
        await this.storage.get("userAuth").then((val: UserAuth) => {
            this.userAuth = val;
        });
        if (this.userAuth) {
            return true;
        }
        this.router.navigate(["/signin"]);
        return false;
    }

}
