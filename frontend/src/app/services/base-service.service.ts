import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";

/**
 * BaseService extends on all services
 */
export abstract class BaseService {

    /**
     * Constructor to call (super()) on other services
     * @param http http client to execute requests
     * @param baseUrlService url of base service
     */
    constructor(
        private http: HttpClient,
        private baseUrlService: string
    ) { }

    /**
     * Method to get full path of an url
     * @param url url to get full path
     * @returns the full path url
     */
    protected getURL(url: string): string {
        return environment.api_url + "/" + this.baseUrlService + "/" + url;
    }

    /**
     * Return on observable of http request with good url
     * @param url url for get
     * @returns an Observable of generic type T
     */
    protected _get<T>(url: string): Observable<T> {
        const headers = new HttpHeaders({ "Content-Type": "application/json" });
        return this.http.get<T>(this.getURL(url), { headers });
    }

    /**
     * Return on observable of http request with good url
     * @param url url for post
     * @param params params for post body
     * @returns an Observable of generic type T
     */
    protected _post<T>(url: string, params?: any): Observable<T> {
        const headers = new HttpHeaders({ "Content-Type": "application/json" });
        return this.http.post<T>(this.getURL(url), params, { headers });
    }

    /**
     * Return on observable of http request with good url
     * @param url url for put
     * @param params params for put body
     * @returns an Observable of generic type T
     */
    protected _put<T>(url: string, params: any): Observable<T> {
        const headers = new HttpHeaders({ "Content-Type": "application/json" });
        return this.http.put<T>(this.getURL(url), params, { headers });
    }

    /**
     * Return on observable of http request with good url
     * @param url url for delete
     */
    protected _delete(url: string, id: number): void {
        const options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
            }),
            body: {
                id: id
            }
        };
        this.http.delete(this.getURL(url), options);
    }

    /**
     * Return on observable of http request with good url
     * @param url url for delete
     */
    protected _deleteObject(url: string, object: object): Observable<any> {
        const options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
            }),
            body: object
        };
        return this.http.delete(this.getURL(url), options);
    }

}
