import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { from, Observable } from "rxjs";
import { mergeMap } from "rxjs/operators";
import { UserAuth } from "src/app/interfaces/iauth";
import { environment } from "src/environments/environment";

/**
 * Token interceptor for JWT
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    /**
     * Constructor of TokenInterceptor
     * @param storage storage to get user auth token
     */
    constructor(
        private storage: Storage
    ) {
    }

    /**
     * Method overrided from HttpInterceptor
     * @param request Request
     * @param next Next
     */
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return from(this.storage.get("userAuth")).pipe(mergeMap((val: UserAuth) => {
            let token = null;
            if (val) {
                token = val.token;
            }

            if (token && request.url.startsWith(environment.api_url)) {
                request = request.clone({
                    setHeaders: {
                        Authorization: `${ val.authType } ${ token }`
                    }
                });
            }

            return next.handle(request);
        }));
    }

}
