import { Injectable } from "@angular/core";
import { Storage } from "@ionic/storage";
import { TranslateService } from "@ngx-translate/core";
import { BehaviorSubject } from "rxjs";
import { AppParameters } from "src/app/interfaces/iparameters";

/**
 * User service to manage user parameters
 */
@Injectable()
export class UserService {

    /**
     * Default user parameters
     */
    public appParameters: BehaviorSubject<AppParameters> = new BehaviorSubject<AppParameters>(null);

    /**
     * Constructor of UserService
     * @param translateService Translate service to set default lang of application
     * @param storage Ionic storage to manage app datas
     */
    constructor(
        private translateService: TranslateService,
        private storage: Storage
    ) {
        this.appParameters.subscribe((appParameters: AppParameters) => {
            if (appParameters) {
                this.translateService.setDefaultLang(appParameters.language);
            }
        });
    }

    /**
     * Set the parameters of application
     * @param appParameters AppParameters to set
     */
    public changeParameters(appParameters: AppParameters) {
        this.appParameters.next(appParameters);
        this.setToStorage("appParameters", appParameters);
    }

    /**
     * Set key value on storage
     * @param key Key of storage
     * @param value Value of storage
     */
    public setToStorage(key: string, value: any) {
        this.storage.set(key, value);
    }

    /**
     * Get value on storage
     * @param key Key of storage to get
     */
    public getToStorage(key: string): Promise<any> {
        return this.storage.get(key);
    }

    /**
     * Remove value on storage
     * @param key Key of storage to remove
     */
    public removeToStorage(key: string) {
        this.storage.remove(key);
    }

}
