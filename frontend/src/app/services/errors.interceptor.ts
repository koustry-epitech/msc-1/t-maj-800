import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AlertController } from "@ionic/angular";
import { TranslateService } from "@ngx-translate/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { AuthService } from "src/app/services/auth.service";

/**
 * Interceptor for all http errors.
 */
@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    /**
     * Boolean to know if we got a forbidden code
     */
    public static forbiddenDetected = false;

    /**
     * Constructor of interceptor.
     * @param userService user service to log out user if api return 401.
     */
    constructor(
        private alertController: AlertController,
        private translateService: TranslateService,
        private authService: AuthService
    ) { }

    /**
     * Default overrided method.
     * @param request Request intercepted.
     * @param next Next request.
     */
    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((err) => {
            const title = this.translateService.instant("ERROR.TITLE") + " " + err.status;
            const msg = this.translateService.instant("ERROR.HTTP_CODE." + err.status);
            if (err.status === 403 && !ErrorInterceptor.forbiddenDetected) {
                this.authService.signout();
                this.showAlert(title, "", msg);
                ErrorInterceptor.forbiddenDetected = true;
            } else if (!ErrorInterceptor.forbiddenDetected) {
                this.showAlert(title, "", msg);
            }
            const error = err.error.message || err.statusText;
            return throwError(error);
        }));
    }

    /**
     * Show an alert with parameters
     * @param title title of alert
     * @param subTitle subtitle of alert
     * @param message message of alert
     * @param buttons buttons of alert
     */
    private async showAlert(title: string, subTitle: string, message: string, buttons: string[] = ["OK"]) {
        const alert = await this.alertController.create({
            header: title,
            subHeader: subTitle,
            message: message,
            buttons: buttons
        });

        await alert.present();
    }

}
