import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { BehaviorSubject, Observable } from "rxjs";
import { LoginInfo, User, UserAuth } from "src/app/interfaces/iauth";
import { BaseService } from "src/app/services/base-service.service";
import { UserService } from "src/app/services/user.service";

/**
 * Auth service to manage auth of app
 */
@Injectable()
export class AuthService extends BaseService {

    /**
     * User authentificated
     */
    public userAuth: BehaviorSubject<UserAuth> = new BehaviorSubject<UserAuth>(null);

    /**
     * Constructor of AuthService
     * @param http HttpClient for BaseService
     */
    constructor(
        http: HttpClient,
        private userService: UserService,
        private router: Router
    ) {
        super(http, "auth");
    }

    /**
     * Sign up a new account
     * @param userAuth User auth info
     */
    public signup(userAuth: User): Observable<UserAuth> {
        return this._post("signup", userAuth);
    }

    /**
     * Method to signin to application
     * @param loginInfo Login info (username + password)
     */
    public signin(loginInfo: LoginInfo): Observable<UserAuth> {
        return this._post("signin", loginInfo);
    }

    /**
     * Method to signout to application
     */
    public signout(): void {
        this.userAuth.next(null);
        this.userService.removeToStorage("userAuth");
        this.router.navigateByUrl("signin");
    }

}
