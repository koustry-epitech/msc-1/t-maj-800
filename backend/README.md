# 🌱 [T-MAJ-800] Voltron - Backend

Specializing in the trading and processing of cereals, a company asked you to enable its customers (cereal growers, farmers . . . ) to view in real time, the state of their agricultural holdings, and to disseminate to them the data con- tinuously.

- **theme**: `greentech`
- **client**: a Céréales Vallée innovation manager
- **objective**: build a full-featured reporting tool for farmers

## 🏗️ Repository architecture 

This repository is architectured in monorepo and is composed of the following packages : 

``` JSON
.
├── README.md           -> documentation 
├── node_modules        -> npm modules   
├── package-lock.json   -> modules structure  
├── package.json        -> project and dependency declaration  
└── src                 -> source files 
```

## 🚀 Run project


Run project localy :
```BASH
npm run start 
```
