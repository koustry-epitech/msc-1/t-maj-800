const path = require('path')

const crypto = require('crypto')

const modulusLength = 4096
const publicKeyEncoding = { type: 'spki', format: 'pem' }
const privateKeyEncoding = { type: 'pkcs8', format: 'pem' }
const { privateKey, publicKey } = crypto.generateKeyPairSync('rsa', { modulusLength, publicKeyEncoding, privateKeyEncoding })

const config = {
  port: process.env.APP_PORT,
  db_port: process.env.DB_PORT,
  db_user: process.env.MONGO_INITDB_ROOT_USERNAME,
  db_pass: process.env.MONGO_INITDB_ROOT_PASSWORD,
  db_name: process.env.MONGO_INITDB_DATABASE,
  db_host: process.env.MONGO_HOST,
  node_env: process.env.NODE_ENV,
  salt: 'APykw4eHbFpUfwrmKbQfeKT8VdBapmXbtEh7hX8SF6q9u7nFVXV69EsJ7By7VeeF',
  saltRounds: 10,
  authPublicKey: publicKey,
  authPrivateKey: privateKey,
  jwtTokenServiceOptions: {
    expiresIn: '1d',
    algorithm: 'RS256',
    issuer: 'backend',
  },
  dataDirPath: path.resolve('./data')
}

module.exports = config
