const CustomError = require('./CustomError')

class JWTTokenServiceError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "JWTTokenServiceError"
    }
}

module.exports = JWTTokenServiceError
