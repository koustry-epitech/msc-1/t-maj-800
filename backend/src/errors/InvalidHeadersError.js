const CustomError = require('./CustomError')

class InvalidHeadersError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "InvalidHeadersError"
    }
}

module.exports = InvalidHeadersError
