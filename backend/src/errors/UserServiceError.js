const CustomError = require('./CustomError')

class UserServiceError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "UserServiceError";
    }
}

module.exports = UserServiceError
