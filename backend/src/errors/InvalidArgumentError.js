const CustomError = require('./CustomError')

class InvalidArgumentError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "InvalidArgumentError"
    }
}

module.exports = InvalidArgumentError
