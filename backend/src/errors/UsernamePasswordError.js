const CustomError = require('./CustomError')

class UsernamePasswordError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "UsernamePasswordError"
    }
}

module.exports = UsernamePasswordError
