const UserServiceError = require('./UserServiceError')
const InvalidHeadersError = require('./InvalidHeadersError')
const InvalidArgumentError = require('./InvalidArgumentError')
const JWTTokenServiceError = require('./JWTTokenServiceError')
const UsernamePasswordError = require('./UsernamePasswordError')
const AuthenticationServiceError = require('./AuthenticationServiceError')

module.exports = {
    UserServiceError,
    InvalidHeadersError,
    InvalidArgumentError,
    JWTTokenServiceError,
    UsernamePasswordError,
    AuthenticationServiceError
}
