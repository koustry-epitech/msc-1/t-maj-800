const CustomError = require('./CustomError')


class AuthenticationServiceError extends CustomError {
    constructor(status, message) {
        super(status, message)
        this.name = "AuthenticationServiceError"
    }
}

module.exports = AuthenticationServiceError
