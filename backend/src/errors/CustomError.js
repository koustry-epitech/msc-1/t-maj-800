class CustomError extends Error {

    constructor(status, message, shouldBePrinted = true) {
        
        if (message === undefined) console.error('Undefined message')
        
        super()
        this.status = status
        this.message = message
        this.shouldBePrinted = shouldBePrinted
    }

}

module.exports = CustomError
