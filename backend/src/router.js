const express = require('express')
const router = express.Router()

// const ErrorMiddleware = require('./middlewares/ErrorMiddleware')

const controllers = require('./controllers/index')


router.use(express.json())
router.use('/auth', controllers.authentication)
router.use('/data', controllers.data)

// router.use(ErrorMiddleware)

module.exports = router
