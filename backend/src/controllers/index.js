const authentication = require("./authenticationController")
const data = require("./dataController")

module.exports = {
    authentication,
    data
}
