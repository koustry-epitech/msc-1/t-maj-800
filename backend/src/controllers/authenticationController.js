const express = require('express')
const router = express.Router()
const httpStatus = require('http-status-codes')

const service = require('../services')


router.post('/signup', async (req, res, next) => {
  try {
    const { token, type, name } = await service.authentication.register(req.body)
    return res.status(httpStatus.CREATED).send({ token, authType: type, name })
  } catch (error) {
    console.error(error)
    return res.status(httpStatus.BAD_REQUEST).send({ error })
  }
})

router.post('/signin', async (req, res, next) => {
  try {
    const { token, type, name } = await service.authentication.login(req.body)
    return res.status(httpStatus.OK).send({ token, type, name })
  } catch (error) {
    return res.status(httpStatus.FORBIDDEN).send({ error })
  }
})

module.exports = router
