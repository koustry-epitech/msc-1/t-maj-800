const express = require('express')
const router = express.Router()
const httpStatus = require('http-status-codes')

const services = require('../services')
const middlewares = require('../middlewares')


router.get('/', middlewares.auth.authMiddleware, async (req, res, next) => {
  try {
    const data = await services.data.getSensorMessagesByQuery(req.query)
    return res.status(httpStatus.OK).send({ data: data})
  } catch (error) {
    return res.status(httpStatus.BAD_REQUEST).send({ error })
  }
})

module.exports = router
