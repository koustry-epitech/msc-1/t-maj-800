const fs = require('fs')
const config = require('../config')

const errors = require('../errors')
const services = require('../services')

const FILE_NAME = 'run.txt'


async function dataExists() {

    try {
        const results = await services.data.getAllSensorMessages()        
        if (results && results.length > 0) return true
        return false
    } catch (error) {
        throw new Error(error)
    }
}

async function loadDataSet() {
    try {
        if (await dataExists()) {
            console.log('=> Data already exists, stopping init data process')
            return
        }
        const fileContent = fs.readFileSync(`${config.dataDirPath}/${FILE_NAME}`)
        const data = JSON.parse(fileContent)
        for (let i = 0; i < data.length; i++) {
            let rawMessages = []
            for (let j = 0; j < data[i].length; j++) {
                let rawMessage = data[i][j]
                if (rawMessage.sensor === 'TempHumiditySensor')
                    rawMessage.data = JSON.parse(rawMessage.data)[0]
                rawMessage.time = (Date.now() + data[i][j].time)
                rawMessages.push(rawMessage)
            }
            console.log(`=> Saving messages batch n° ${i + 1}/${data.length}`)
            const res = await services.data.saveAll(rawMessages)
        }
    } catch (error) {
        console.error(error.message)
    }
}

async function initialize() {
    console.log(`Initializing data`)
    await loadDataSet()
}

module.exports = {
    initialize
}
