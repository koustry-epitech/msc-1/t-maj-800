const mongoose = require('mongoose')


User = new mongoose.Schema({
    name: {
        type: String,
        unique: false,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    }
})

User.query.byUserId = function(id) {
    return this.where({ _id: id });
}

module.exports = mongoose.model('User', User);
