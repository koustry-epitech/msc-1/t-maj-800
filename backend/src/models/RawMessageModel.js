const mongoose = require('mongoose')


RawMessage = new mongoose.Schema({
    sensor: {
        type: String,
        enum: [
            'Photoresistor',
            'TempHumiditySensor',
            'waterLevelSensor',
            'flameSensor'
        ],
        unique: false,
        required: true
    },
    time: {
        type: Date,
        unique: false,
        required: true
    },
    data: {
        type: Number,
        unique: false,
        required: true
    }
})

RawMessage.query.getRawMessageById = function(id) {
    return this.where({ _id: id })
}

module.exports = mongoose.model('RawMessage', RawMessage)
