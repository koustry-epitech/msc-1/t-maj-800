const httpStatus = require('http-status-codes')

const errors = require("../errors")
const services = require("../services")


async function authMiddleware(req, res, next) {
    try {
        if (!req.headers.authorization || req.headers.authorization.split(' ')[0] !== "Bearer" && req.headers.authorization.split(' ').length !== 2)
            throw new errors.InvalidHeadersError(httpStatus.FORBIDDEN, "The authorization header is not correct")
        const token = req.headers.authorization.split(' ')[1]
        const { sub } = await services.jwtTokenService.verifyJWT(token)
        const user = await services.user.loadUserById({ userId: sub })

        res.locals.user = user
        return next()
    } catch (error) {
        console.error(error)
        return next(error)
    }
}

module.exports = {
    authMiddleware
}
