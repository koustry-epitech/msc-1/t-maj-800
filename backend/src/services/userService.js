const bcrypt = require('bcrypt')
const httpStatus = require('http-status-codes')

const config = require('../config')
const model = require('../models')
const errors = require('../errors')


async function createUser({ name, email, password }) {
    if (!name || !email || !password) throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    const params = {
        name: name,
        email: email,
        username: email,
        password: bcrypt.hashSync(password + config.salt, config.saltRounds)
    }
    try {
        const user = await new model.User(params).save()
        return user
    } catch (error) {
        throw new errors.UserServiceError(httpStatus.INTERNAL_SERVER_ERROR, error.message)
    }
}

async function loadUserByUsername({ username }) {
    if (!username) throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        const user = await model.User.findOne({ username: username })
        if (!user) throw new errors.UserServiceError(httpStatus.NOT_FOUND, `User with username ${username} was not found`)
        return user
    } catch (error) {
        throw error
    }
}

async function loadUserById({ userId }) {    
    if (!userId) throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        const user = await model.User.findById(userId)
        if (!user) throw new errors.UserServiceError(httpStatus.NOT_FOUND, `User with id ${userId} was not found`)
        return user
    } catch (error) {
        throw new errors.UserServiceError(httpStatus.NOT_FOUND, error.message)
    }
}

module.exports = {
    createUser,
    loadUserByUsername,
    loadUserById
}
