const bcrypt = require('bcrypt')
const httpStatus = require('http-status-codes')

const config = require('../config')
const errors = require('../errors')
const userService = require('./userService')
const jwtTokenService = require('./jwtTokenService')


async function register({ name, email, password }) {
    if (!name || !email || !password)
        throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        const user = await userService.createUser({ name, email, password })
        const credentials = { username: user.username, password: password }
        return await login(credentials)
    } catch (error) {
        throw new errors.AuthenticationServiceError(httpStatus.BAD_REQUEST, error.message)
    }
}

async function login({ username, password }) {
    if (!username || !password)
        throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        const user = await userService.loadUserByUsername({ username })

        const passwordMatch = bcrypt.compareSync(password + config.salt, user.password)
        if (!passwordMatch) throw new errors.UsernamePasswordError(`Passwords does not match`)
    
        const generateJWTParams = {
            payload: {
                name: user.name,
                email: user.email,
                username: user.username
            },
            audience: 'frontend',
            subject: String(user._id)
        }
        const jwt = jwtTokenService.generateJWT(generateJWTParams)
        return { token: jwt, type: 'Bearer', name: user.name }
    } catch (error) {
        throw error
    }

}

module.exports = {
    register,
    login
}
