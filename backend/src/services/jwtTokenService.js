const jwt = require('jsonwebtoken')
const httpStatus = require('http-status-codes')

const config = require('../config')
const errors = require('../errors')


function generateJWT({ payload, audience, subject }) {
    const options = {
        audience: audience,
        subject: subject,
        ...config.jwtTokenServiceOptions
    }
    try {
        return jwt.sign(payload, config.authPrivateKey, options)
    } catch (error) {
        throw new errors.JWTTokenServiceError(httpStatus.FORBIDDEN, error.message)
    }
}

function verifyJWT(token) {
    try {
        return jwt.verify(token, config.authPublicKey, { audience: 'frontend', algorithms: ['RS256'] })
    } catch (error) {
        throw new errors.JWTTokenServiceError(httpStatus.FORBIDDEN, error.message)
    }
}

module.exports = {
    generateJWT,
    verifyJWT
}
