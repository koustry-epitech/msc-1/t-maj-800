const httpStatus = require('http-status-codes')

const errors = require('../errors')
const models = require('../models')


async function saveAll(data) {
    if (!Array.isArray(data) && data.length === 0)
        throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    const entries = []
    for (let i = 0 ; i < data.length ; i++ ) {
        try {
            const rawMessage = data[i]
            const entry = await save(rawMessage)
            entries.push(entry)
        } catch (error) {
            throw new Error(error)
        }
    }
    return entries
}

async function save({ sensor, time, data }) {    
    if (sensor === undefined || time === undefined || data === undefined)
        throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        return await new models.RawMessageModel({ sensor, time, data }).save()            
    } catch (error) {
        throw new Error(error)
    }
}

async function getSensorMessagesByQuery(query) {
    if (query === undefined)
        throw new errors.InvalidArgumentError(httpStatus.BAD_REQUEST, 'Invalid arguments')

    try {
        return await models.RawMessageModel.find(query)
    } catch (error) {
        throw new Error(error)
    }
}

async function getAllSensorMessages() {
    try {
        return await models.RawMessageModel.find()
    } catch (error) {
        throw new Error(error)
    }
}

module.exports = {
    saveAll,
    save,
    getSensorMessagesByQuery,
    getAllSensorMessages
}
