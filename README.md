# 🌱 [T-MAJ-800] Voltron

Specializing in the trading and processing of cereals, a company asked you to enable its customers (cereal growers, farmers . . . ) to view in real time, the state of their agricultural holdings, and to disseminate to them the data con- tinuously.

- **theme**: `greentech`
- **client**: a Céréales Vallée innovation manager
- **objective**: build a full-featured reporting tool for farmers

## 🏗️ Repository architecture 

This repository is architectured in monorepo and is composed of the following packages : 

``` BASH 
.
├── README.md   -> documentation 
├── backend     -> nodejs web server  
├── frontend    -> ionic mobile app 
└── iot         -> IoT sensors code
```

## 🤖 Git Workflow 

This part refers to the git workflow and code management that team have to apply. 

### Branching Guidelines 

The Git workflow is setup as follow and allows the team to manage each development cycle :

- Production branch
- Development branch
- Features and fixes branches

**Branches management should respect the following table, each merge from `feat` or `fix` to `develop` must pass from Merge Request.**

| Stage       | Branch                      | Description, Instructions, Notes                         |
| ----------- | --------------------------- | -------------------------------------------------------- |
| Production  | master                      | Accepts merges from `develop` with Merge Request         |
| Development | develop                     | Accepts merges from `feat` / `fix` without Merge Request |
| Features    | feat/\<package>/\<name>     | Always branch from `develop` and merge to `develop`      |
| Fixes       | fix/\<package>              | Always branch off `master`                               |

Samples :
``` BASH 
feat/backend/controllers 
```

``` BASH 
fix/frontend
```

### Commit Message Guidelines 

Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

``` BASH
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional. 

Samples : 
``` BASH
docs(changelog): update changelog to beta.5
```
``` BASH 
fix(release): need to depend on latest rxjs and zone.js
```

### Scopes 
Must be one of the following:

* **build**: Changes that affect the build system or dependencies 
* **ci**: Changes to our CI configuration files and scripts
* **docs**: Documentation only changes 
* **feat**: A new feature 
* **fix**: A bug fix 
* **perf**: A code change that improves performance 
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, etc)
* **test**: Adding missing tests or correcting existing tests

## 📝 Coding Conventions

Each package have to follow best practices related to the language and framework that has been used : 
