#include "./includes/core.h"
//photoResistor photo;
Core *_core;
void setup()
{
  Serial.begin(9600);
  //photo = new photoResistor("photoResistor", A0);
  _core = new Core();
  _core->setup();
}

void loop()
{
  // put your main code here, to run repeatedly:
  _core->start(); // lancer la lecture de la sortie des capteurs
  delay(1000);
}
