#include "../includes/core.h"

Core::Core()
{
}

Core::~Core()
{
}

void Core::setup()
{
    _lcd = new lcd();
    buttonDisplay = new button(8);
    i = 0;
    photo = new photoResistor("Photoresistor");
    tempHumi = new tempHumidity("TempHumidity");
    flame = new flameSensor("flameSensor");
    water = new waterLevelSensor("waterLevel");

    photo->setPinAnalog(A0);
    tempHumi->setPinDigital(13);
    water->setPinAnalog(A2);
    flame->setPinAnalog(A1);
    flame->setPinDigital(12);

    _sensors = { .length = 5, .data = { photo, tempHumi, water, flame } };
}

void Core::start()
{
    Serial.print("[");
    Serial.print(_sensors.data[0]->getJson()+ ",");
    Serial.print(_sensors.data[1]->getJson()+ ",");
    Serial.print(_sensors.data[2]->getJson()+ ",");
    Serial.print(_sensors.data[3]->getJson());
    Serial.print("]\n");
    if (buttonDisplay->getValue() == 0){
        if (i < 4){
            i++;
        } else {
            i = 0;
        }
    }
    if (i == 3)
     _lcd->display(tempHumi->getSensorName() + "\n" + String(tempHumi->getValueSensorHumidity()) + split(tempHumi->getSign(),',',1));
    else
     _lcd->display(_sensors.data[i]->getSensorName() + "\n" + String(_sensors.data[i]->getValueSensorFloat()) + split(_sensors.data[i]->getSign(),',',0));

}

String Core::split(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}