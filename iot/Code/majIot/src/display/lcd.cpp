#include "../../includes/display/lcd.h"

const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2;

LiquidCrystal myLcd(rs, en, d4, d5, d6, d7);

lcd::lcd(){
    myLcd.begin(16, 2);
}

lcd::~lcd(){

}

void lcd::display(String value){
    int i = 0;
    String part = "";

    clear();

    part = split(value, '\n', 0);
    myLcd.setCursor(0,0);
    myLcd.print(part);

    part = split(value, '\n', 1);
    myLcd.setCursor(0,1);
    myLcd.print(part);
}

 void lcd::clear(){
   myLcd.clear();
 }

String lcd::split(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }

  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}