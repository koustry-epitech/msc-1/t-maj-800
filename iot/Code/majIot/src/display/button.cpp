#include "../../includes/display/button.h"

button::button(int pin)
{
    this->pin = pin;
    pinMode(this->pin, INPUT);
}

button::~button()
{
}

int button::getValue()
{
    return digitalRead(this->pin);
}