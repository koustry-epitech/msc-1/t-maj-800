#include "../../includes/sensors/flameSensor.h"

flameSensor::flameSensor(String name)
{
    sensorName = name;
    sign = "F";
}

flameSensor::~flameSensor()
{
}

void flameSensor::setPinAnalog(int pin)
{
    sensorPinAnalog = pin;
}

void flameSensor::setPinDigital(int pin){
    sensorPinDigital = pin;
    pinMode(sensorPinDigital, INPUT);
}


float flameSensor::getValueSensorFloat()
{
    return analogRead(this->sensorPinAnalog);
}

String flameSensor::getSensorName()
{
    return (sensorName);
}

String flameSensor::getJson()
{
    String result;
    DynamicJsonDocument doc(1024);

    doc["sensor"] = getSensorName();
    doc["time"] = millis();
    doc["data"] = getValueSensorFloat();

    serializeJson(doc, result);
    return (result);
}

String flameSensor::getSign(){
    return (sign);
}
