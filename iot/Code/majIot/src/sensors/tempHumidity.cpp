#include "../../includes/sensors/tempHumidity.h"

DHT dht(13,DHT11);

tempHumidity::tempHumidity(String name)
{
    sensorName = name;
    sign = "C,%";
    dht.begin();
}

tempHumidity::~tempHumidity()
{
}

void tempHumidity::setPinDigital(int pin){
    sensorPinDigital = pin;
}

void tempHumidity::setPinAnalog(int pin){ }

float tempHumidity::getValueSensorFloat()
{
    return dht.readTemperature();
}

float tempHumidity::getValueSensorHumidity() {
    return dht.readHumidity();
}

String tempHumidity::getSign()
{
    return (sign);
}

String tempHumidity::getSensorName(){
    return(sensorName);
}

String tempHumidity::convertFloatString(float value){
    String valString;

    valString = String(value);
    return(valString);
}

String tempHumidity::getJson()
{
    String result;
    DynamicJsonDocument doc(1024);
    String dataSensor[2];

    dataSensor[0] = convertFloatString(getValueSensorFloat());
    dataSensor[1] = convertFloatString(getValueSensorHumidity());
    doc["sensor"] = getSensorName();
    doc["time"] = millis();
    doc["data"] = "[" + dataSensor[0] + "," + dataSensor[1] + "]";

    serializeJson(doc, result);
    return (result);
}