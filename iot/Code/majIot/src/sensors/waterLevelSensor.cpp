#include "../../includes/sensors/waterLevelSensor.h"

waterLevelSensor::waterLevelSensor(String name)
{
    sensorName = name;
    sign = "/ 350";
}

waterLevelSensor::~waterLevelSensor()
{
}

void waterLevelSensor::setPinAnalog(int pin)
{
    sensorPinAnalog = pin;
}

void waterLevelSensor::setPinDigital(int pin){}


float waterLevelSensor::getValueSensorFloat()
{
    return analogRead(this->sensorPinAnalog);
}

String waterLevelSensor::getSensorName()
{
    return (sensorName);
}

String waterLevelSensor::getJson()
{
    String result;
    DynamicJsonDocument doc(1024);

    doc["sensor"] = getSensorName();
    doc["time"] = millis();
    doc["data"] = getValueSensorFloat();

    serializeJson(doc, result);
    return (result);
}

String waterLevelSensor::getSign(){
    return (sign);
}