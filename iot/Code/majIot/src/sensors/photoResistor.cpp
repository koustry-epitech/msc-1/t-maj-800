#include "../../includes/sensors/photoResistor.h"

photoResistor::photoResistor(String name)
{
    sensorName = name;
    sign = " /1024";
}

photoResistor::~photoResistor()
{
}

void photoResistor::setPinAnalog(int pin)
{
    sensorPinAnalog = pin;
}

void photoResistor::setPinDigital(int pin){}


float photoResistor::getValueSensorFloat()
{
    return analogRead(this->sensorPinAnalog);
}

String photoResistor::getSensorName()
{
    return (sensorName);
}

String photoResistor::getJson()
{
    String result;
    DynamicJsonDocument doc(1024);

    doc["sensor"] = getSensorName();
    doc["time"] = millis();
    doc["data"] = getValueSensorFloat();

    serializeJson(doc, result);
    return (result);
}

String photoResistor::getSign(){
    return (sign);
}