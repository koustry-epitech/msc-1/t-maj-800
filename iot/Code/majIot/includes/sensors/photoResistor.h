#ifndef PHOTORESISTOR_H
#define PHOTORESISTOR_H

#include "../interfaces/sensors.h"
#include <Arduino.h>

class photoResistor : public Sensor
{
public:
    photoResistor(String);
    ~photoResistor();
    void setPinDigital(int);
    void setPinAnalog(int);
    float getValueSensorFloat();
    String getSensorName();
    String getJson();
    String getSign();

private:
    String sensorName;
    float sensorValue;
    int sensorPinAnalog;
    String sign;
};

#endif