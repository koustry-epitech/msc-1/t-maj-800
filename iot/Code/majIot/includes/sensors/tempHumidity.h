#ifndef TEMPHUMIDITY_H
#define TEMPHUMIDITY_H

#include "../interfaces/sensors.h"
#include <Arduino.h>
#include <DHT.h>

class tempHumidity : public Sensor
{
public:
    tempHumidity(String);
    ~tempHumidity();
    void setPinDigital(int);
    void setPinAnalog(int);
    float getValueSensorFloat();
    float getValueSensorHumidity();
    String getSensorName();
    String getSign();

    String getJson();
    String convertFloatString(float);
    
private:
    String sign;
    String sensorName;
    float sensorValue;
    int sensorPinDigital;
    //DHT *dht(int, int);
};

#endif