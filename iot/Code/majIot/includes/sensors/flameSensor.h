#ifndef FLAMESENSOR_H
#define FLAMESENSOR_H

#include "../interfaces/sensors.h"
#include <Arduino.h>

class flameSensor : public Sensor
{
public:
    flameSensor(String);
    ~flameSensor();
    void setPinDigital(int);
    void setPinAnalog(int);
    float getValueSensorFloat();
    String getSensorName();
    String getJson();
    String getSign();

private:
    String sensorName;
    float sensorValue;
    int sensorPinDigital;
    int sensorPinAnalog;
    String sign;
};

#endif