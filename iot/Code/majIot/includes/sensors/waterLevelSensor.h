#ifndef WATERLEVELSENSOR_H
#define WATERLEVELSENSOR_H

#include "../interfaces/sensors.h"
#include <Arduino.h>

class waterLevelSensor : public Sensor
{
public:
    waterLevelSensor(String);
    ~waterLevelSensor();
    void setPinDigital(int);
    void setPinAnalog(int);
    float getValueSensorFloat();
    String getSensorName();
    String getJson();
    String getSign();
    
private:
    String sensorName;
    float sensorValue;
    int sensorPinAnalog;
    String sign;
};

#endif