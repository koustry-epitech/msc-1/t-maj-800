#ifndef CORE_H
#define CORE_H
#include <Arduino.h>

#include "./interfaces/sensors.h"
#include "../includes/sensors/photoResistor.h"
#include "../includes/sensors/tempHumidity.h"
#include "../includes/display/button.h"
#include "../includes/display/lcd.h"
#include "../includes/sensors/flameSensor.h"
#include "../includes/sensors/waterLevelSensor.h"
#include "./interfaces/List.h"
#include "./display/lcd.h"

//using namespace std;
// Base class
class Core
{
public:
    Core();
    ~Core();
    void setup();
    void start();
    String split(String, char, int);
private:
    photoResistor *photo;
    tempHumidity *tempHumi;
    flameSensor *flame;
    waterLevelSensor *water;
    List _sensors;
    lcd *_lcd;
    button *buttonDisplay;
    int i;
};

#endif