#ifndef SENSOR_H
#define SENSOR_H

#include <Arduino.h>
#include <ArduinoJson.h>


#define TIME_MSG_LEN 11 // time sync to PC is HEADER and unix time_t as ten ascii digits
#define TIME_HEADER 255 // Header tag for serial time sync message

// Base class
class Sensor
{
public:
    Sensor() {}
    virtual void setPinDigital(int) = 0;
    virtual void setPinAnalog(int) = 0;
    // virtual int getValueSensorInt();
    virtual float getValueSensorFloat() = 0;
    virtual String getSensorName() = 0;
    virtual String getJson() = 0;
    virtual String getSign() = 0;

private:
    String sensorName;
    int sensorPinDigital;
    float sensorValue;
    int sensorPinAnalog;
    String sign;
};

#endif