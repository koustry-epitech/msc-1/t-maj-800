#ifndef LIST_H
#define LIST_H

#include <Arduino.h>
#include "./sensors.h"

class List {
public:
    int length;
    Sensor* data[6];
    void append(Sensor* item, int pos) {
        if (pos < length){
            data[pos] = item;
        }
    }
    void remove(byte index) {
        if (index >= length) return;
        memmove(&data[index], &data[index+1], length - index - 1);
        length--;
    }
};

#endif