#ifndef LCD_H
#define LCD_H

#include <Arduino.h>
#include <LiquidCrystal.h>

class lcd
{
public:
    lcd();
    ~lcd();
    void display(String);
    void clear();
    String split(String, char, int);
};

#endif