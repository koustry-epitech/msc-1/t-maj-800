#ifndef BUTTON_H
#define BUTTON_H
#include <Arduino.h>

class button
{
public:
    button(int);
    ~button();
    int getValue();
private:
    int pin;
};

#endif